package Types;

import javafx.beans.property.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Lachlan Stevens on 31/03/2016 at 10:59 PM.
 *
 * An abstraction of an item. Not only is this the instance
 */
public class Item{


    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleIntegerProperty price = new SimpleIntegerProperty();
    private Wear wear = null;

    public Item(String id){
        this.id = new SimpleStringProperty(id);
    }


    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public final StringProperty idProperty() { return id; }


    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public final StringProperty nameProperty() { return name; }


    public int getPrice() {
        return price.get();
    }

    public void setPrice(int price) {
        this.price.set(price);
    }

    public final StringProperty priceProperty() {
        BigDecimal d = new BigDecimal(price.get());
        d = d.divide(new BigDecimal(100));
        d = d.setScale(2, RoundingMode.CEILING);

        return new SimpleStringProperty(d.toString());
    }


    // In format "<item name> | <wear>"
    // Or if its an item with no wear, just the name
    public SimpleStringProperty getDisplayString(){
        if (wear.wear_amount.equals("NULL")){return name;}
        return new SimpleStringProperty(name + " | " + wear.wear_amount);
    }

}