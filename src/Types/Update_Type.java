package Types;

/**
 * Created by Lachlan Stevens on 31/03/2016 at 11:07 PM.
 *
 * A very simple type which details what type of update it is.
 */
public enum Update_Type {

    SELL("SELL"), OFFER("OFFER"), CANCEL("CANCEL"), TRADE("TRADED");

    public String action;

    Update_Type(String action){
        this.action = action;
    }

}
