package Types;

import Utility.Settings;
import Utility.Web_Connections;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Lachlan Stevens on 3/04/2016 at 5:22 PM.
 */
public class Inventory {

    public HashMap<String, Item> ITEMS = new HashMap<>();
    public HashMap<String, Integer> COUNT = new HashMap<>();

    // Empty Constructor in case its useful to have one
    public Inventory(){

    }

    public Inventory(JSONObject json) throws JSONException, IOException {
        // TODO: remember to uncomment this when you have net

        JSONArray jsonItems = json.getJSONObject("result").getJSONArray("items");

        JSONObject weaponSchema = Web_Connections.fetch(new URL("http://api.steampowered.com/IEconItems_730/GetSchema/v0002/?key=8C6D423444EA175D01CF6C54AF6A88DE&format=json"), "GET", "");

        int inventoryItems = jsonItems.length();

        for (int i = 0; i < inventoryItems; i++){
            String itemType = "";

            JSONObject j = jsonItems.getJSONObject(i);
            int id = j.getInt("id");
            Item item = new Item(Integer.toString(id));

            int itemTypeIndex = j.getInt("defindex");
            JSONArray itemArray = weaponSchema.getJSONObject("result").getJSONArray("items");
            for (int k = 0; k < itemArray.length(); k++){
                int index = itemArray.getJSONObject(k).getInt("defindex");
                if (itemTypeIndex == index){
                    itemType = itemArray.getJSONObject(k).getString("name");
                }
            }

            item.setName(Settings.translateSchemaName(itemType));
            ITEMS.put(Integer.toString(id), item);

        }

        countItems();


    }

    public int size(){
        return ITEMS.size();
    }

    public void countItems(){
        for (Item i : ITEMS.values()){
            if (!COUNT.containsKey(i.getName())){
                COUNT.put(i.getName(), 1);
            } else {
                COUNT.put(i.getName(), COUNT.get(i.getName())+1);
            }
        }
    }

}
