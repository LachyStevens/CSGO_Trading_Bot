package Types;

/**
 * Created by Lachlan Stevens on 1/04/2016 at 8:41 PM.
 */
public enum Wear {

    FACTORY_NEW("Factory New"),
    MINIMAL_WEAR("Minimal Wear"),
    FIELD_TESTED("Field Tested"),
    WELL_WORN("Well Worn"),
    BATTLE_SCARRED("Battle Scarred"),
    NONE("NULL");

    public String wear_amount;

    Wear(String wear_amount){
        this.wear_amount = wear_amount;
    }

}
