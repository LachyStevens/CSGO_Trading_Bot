package GUI;


import Utility.Update;
import javafx.scene.control.TextArea;

/**
 * Created by Lachlan Stevens on 31/03/2016 at 10:06 PM.
 *
 * The aim for this class is to log information on sales and offers. Every time a trade information is received in a update
 * tick, it is added into a log. This log will show the date and time, item, and whether or not it was sold or offered and for what price.
 * Every day a new file will be created to make it easier to find something if you're looking for it. Each file will be held for 7 days.
 */

public class Activity_Log extends TextArea {

    private String content;

    public Activity_Log(){
        super();
        content = "";
        this.setEditable(false);
        this.setFocusTraversable(false);


    }

    public void addUpdate(Update update){
        content += update.toString() + "\n";
        this.setText(content);
        this.setScrollTop(Double.MAX_VALUE);
    }

}
