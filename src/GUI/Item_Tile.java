package GUI;

import Types.Item;
import Utility.Image_Reader;
import javafx.scene.Parent;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;

import static javafx.scene.layout.Region.USE_PREF_SIZE;

/**
 * Created by Lachlan Stevens on 6/04/2016 at 8:40 PM.
 */
public class Item_Tile extends Parent {

    public Item item;
    final ImageView view;
    final StackPane stack = new StackPane();
    final Rectangle rect = new Rectangle(100,100);

    public Item_Tile(Item item){
        this.item = item;

        view = new ImageView(Image_Reader.ALL_IMAGES.get(item.getName()));
        view.setFitHeight(100);
        view.setFitWidth(100);

        stack.setPrefSize(100,100);
        stack.setMinSize(USE_PREF_SIZE,USE_PREF_SIZE);
        stack.setMinSize(USE_PREF_SIZE,USE_PREF_SIZE);
        stack.getChildren().addAll(rect, view);

        this.getChildren().add(stack);
    }



}
