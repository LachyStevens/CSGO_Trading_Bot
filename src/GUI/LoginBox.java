package GUI;

import Utility.Image_Reader;
import Utility.Settings;
import Utility.Web_Connections;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.net.CookieStore;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import static javafx.scene.layout.Region.USE_PREF_SIZE;

public class LoginBox extends Parent {

    Stage stage;
    Pane root;
    TextField username;
    PasswordField password;
    Label userLabel = new Label("Username: "), passLabel = new Label("Password: ");
    Button login, cancel;

    public LoginBox(){

        stage = new Stage();

        initGUI();

        stage.setTitle("Log In");
        stage.getIcons().add(Image_Reader.get("KeyTrade"));
        stage.setScene(new Scene(root, 400, 180));
        stage.setResizable(false);

        stage.show();
    }

    private void initGUI(){

        root = new Pane();

        username = new TextField();
        username.setText(Settings.USERNAME);
        username.setLayoutX(145);
        username.setLayoutY(30);
        username.setPrefSize(200, 25);
        username.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        username.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        username.setOnKeyPressed(e -> {

            if (e.getCode().equals(KeyCode.ENTER)) {
                try {

                    attemptLogin();

                } catch (IOException e1) {

                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Error");
                    alert.setHeaderText("Denied response from login endpoint!");
                    alert.setContentText(e1.getMessage());
                    alert.showAndWait();

                }
            }
        });

        password = new PasswordField();
        password.setText(Settings.PASSWORD);
        password.setLayoutX(145);
        password.setLayoutY(85);
        password.setPrefSize(200, 25);
        password.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        password.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        password.setOnKeyPressed(e -> {

            if (e.getCode().equals(KeyCode.ENTER)) {
                try {

                    attemptLogin();

                } catch (IOException e1) {

                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Error");
                    alert.setHeaderText("Denied response from login endpoint!");
                    alert.setContentText(e1.getMessage());
                    alert.showAndWait();

                }
            }
        });



        userLabel.relocate(75, 35);

        passLabel.relocate(75, 90);

        login = new Button("Log In");
        login.setLayoutX(30);
        login.setLayoutY(140);
        login.setPrefWidth(150);
        login.setMinWidth(USE_PREF_SIZE);
        login.setMaxWidth(USE_PREF_SIZE);
        login.defaultButtonProperty().bind(login.focusedProperty());
        login.setOnMouseClicked(e -> {
            try {

                attemptLogin();

            } catch (IOException e1){

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Error");
                alert.setHeaderText("Denied response from login endpoint!");
                alert.setContentText(e1.getMessage());
                alert.showAndWait();

            }
        });

        cancel = new Button("Cancel");
        cancel.setLayoutX(220);
        cancel.setLayoutY(140);
        cancel.setPrefWidth(150);
        cancel.setMinWidth(USE_PREF_SIZE);
        cancel.setMaxWidth(USE_PREF_SIZE);
        cancel.setOnMouseClicked(e -> stage.close());

        root.getChildren().addAll(username, password, userLabel, passLabel, login, cancel);

    }

    public Map<String, String> attemptLogin() throws IOException{

        JSONObject loginResponse = null;
        /*
        try {


            System.out.println(loginResponse);

        } catch (JSONException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | BadPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        /* TODO: read through successful login output and check if the login is successful here. I may want to return the cookies
        that I steal from the output - null if the login was unsuccessful. (Maybe in Map form)
         */

        return new HashMap<>();

    }

}
