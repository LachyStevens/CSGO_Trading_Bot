package GUI;

import Utility.File_IO;
import Utility.Image_Reader;
import Utility.Settings;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.yaml.snakeyaml.Yaml;
import uk.co.thomasc.scrapbanktf.Bot;
import uk.co.thomasc.scrapbanktf.BotInfo;
import uk.co.thomasc.scrapbanktf.util.SQL;

import java.util.*;


/**
 * Created by Lachlan Stevens on 6/04/2016 at 1:33 PM.
 */

public class Main extends Application {

    final Menu settings = new Menu("Settings");
    final MenuItem login = new MenuItem("Login");
    final MenuItem setVars = new MenuItem("Set Environment Variables");

    static Bot bot;
    static Main main;
    public static SQL sql = new SQL("jdbc:mysql://localhost/botSql");

    Pane root;
    Sell_Pane sell_pane;
    MenuBar menu_bar;
    LoginBox login_box;
    VarSetBox variable_set_box;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("KeyTrade " + Settings.VERSION);

        root = new Pane();

        onStartup();

        initGUI();

        primaryStage.setScene(new Scene(root, 920, 700));
        primaryStage.getIcons().add(Image_Reader.get("KeyTrade"));
        primaryStage.setResizable(false);
        primaryStage.show();


    }

    private void initGUI() {

        menu_bar = new MenuBar(settings);
        menu_bar.prefWidthProperty().bind(root.widthProperty());

        settings.getItems().addAll(login, setVars);

        login.setOnAction(e -> login_box = new LoginBox());
        setVars.setOnAction(e -> variable_set_box = new VarSetBox());

        sell_pane = new Sell_Pane();

        root.getChildren().addAll(sell_pane, menu_bar);
    }

    private void onStartup() {
        File_IO.loadFiles(); // Load all the saved logs
        Settings.load();
        //sell_pane.cleanActivityLog(); // Remove old entries in activity log
        //launchBot();
        // TODO: Create timer that saves the activity log every second - or find a way to execute code when closing a program EVERY TIME

    }

    private void launchBot() {

        final Yaml yaml = new Yaml();

        List<Long> id = new ArrayList<>();
        id.add(Long.parseLong(Settings.STEAM64_ID));
        BotInfo.setAdmins(id);

        BotInfo.setApiKey(Settings.STEAM_API_KEY);

        Map<String, Object> inf = new HashMap<>();
        inf.put("username", Settings.USERNAME);
        inf.put("password", Settings.PASSWORD);
        inf.put("id", 1);
        inf.put("displayname", "test");

        new Thread(new BotThread(new BotInfo(inf))).start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private class BotThread implements Runnable {

        private final BotInfo info;

        public BotThread(BotInfo info) {
            this.info = info;
        }

        @Override
        public void run() {
            int crashes = 0;
            while (crashes < 1000) {
                try {
                    bot = new Bot(info);
                } catch (Exception e) {
                    String error = "Unhandled error on bot " +  crashes++ + "\n" + e.getClass() + " at";
                    for (StackTraceElement el : e.getStackTrace()) {
                        error += "\n" + el;
                    }
                }
            }
        }

    }

}