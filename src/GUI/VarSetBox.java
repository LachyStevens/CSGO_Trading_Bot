package GUI;

import Utility.Image_Reader;
import Utility.Settings;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import static javafx.scene.layout.Region.USE_PREF_SIZE;

public class VarSetBox extends Parent {

    Stage stage;
    Pane root;
    TextField username, steamapi, steam64, opskins;
    Label   steamapiLabel = new Label("Steam Api Key: "),
            steam64Label = new Label("Steam64 ID: "),
            opskinsLabel = new Label("OPSkins Api Key: "),
            userLabel = new Label("Username: "),
            passLabel = new Label("Password: ");
    PasswordField password;
    Button save, cancel;
    CheckBox saveUserAndPass = new CheckBox("Save Username and Password?");

    public VarSetBox(){

        stage = new Stage();

        initGUI();

        stage.setTitle("Set Environment Variables");
        stage.getIcons().add(Image_Reader.get("KeyTrade"));
        stage.setScene(new Scene(root, 400, 350));
        stage.setResizable(false);

        stage.show();
    }

    private void initGUI(){

        root = new Pane();

        steamapi = new TextField();
        steamapi.setText(Settings.STEAM_API_KEY);
        steamapi.setLayoutX(145);
        steamapi.setLayoutY(30);
        steamapi.setPrefSize(200, 25);
        steamapi.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        steamapi.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        steam64 = new TextField();
        steam64.setText(Settings.STEAM64_ID);
        steam64.setLayoutX(145);
        steam64.setLayoutY(85);
        steam64.setPrefSize(200, 25);
        steam64.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        steam64.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        opskins = new TextField();
        opskins.setText(Settings.OPSKINS_API_KEY);
        opskins.setLayoutX(145);
        opskins.setLayoutY(140);
        opskins.setPrefSize(200, 25);
        opskins.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        opskins.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        username = new TextField();
        username.setText(Settings.USERNAME);
        username.setLayoutX(145);
        username.setLayoutY(195);
        username.setPrefSize(200, 25);
        username.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        username.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        password = new PasswordField();
        password.setText(Settings.PASSWORD);
        password.setLayoutX(145);
        password.setLayoutY(250);
        password.setPrefSize(200, 25);
        password.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        password.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        steamapiLabel.relocate(30, 30);

        steam64Label.relocate(30, 85);

        opskinsLabel.relocate(30, 140);

        userLabel.relocate(30, 195);

        passLabel.relocate(30, 250);

        if (!Settings.USERNAME.isEmpty() && !Settings.PASSWORD.isEmpty()) saveUserAndPass.setSelected(true);
        saveUserAndPass.setLayoutX(110);
        saveUserAndPass.setLayoutY(290);

        save = new Button("Save");
        save.setLayoutX(30);
        save.setLayoutY(325);
        save.setPrefWidth(150);
        save.setMinWidth(USE_PREF_SIZE);
        save.setMaxWidth(USE_PREF_SIZE);
        save.setOnMouseClicked(e -> {

            boolean userAndPass = saveUserAndPass.isSelected();

            Settings.STEAM_API_KEY = steamapi.getText();
            Settings.STEAM64_ID = steam64.getText();
            Settings.OPSKINS_API_KEY = opskins.getText();
            if (userAndPass) {
                Settings.USERNAME = username.getText();
                Settings.PASSWORD = password.getText();
            }
            Settings.save();
            stage.close();

        });

        cancel = new Button("Cancel");
        cancel.setLayoutX(220);
        cancel.setLayoutY(325);
        cancel.setPrefWidth(150);
        cancel.setMinWidth(USE_PREF_SIZE);
        cancel.setMaxWidth(USE_PREF_SIZE);
        cancel.setOnMouseClicked(e -> stage.close());

        root.getChildren().addAll(steamapi, steam64, opskins, username, password, opskinsLabel, steam64Label, steamapiLabel, userLabel, passLabel, save, cancel, saveUserAndPass);

    }

}
