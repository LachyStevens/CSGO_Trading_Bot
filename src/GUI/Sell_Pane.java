package GUI;

import Types.Inventory;
import Types.Item;
import Types.Update_Type;
import Utility.Settings;
import Utility.Update;
import Utility.Web_Connections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

import static javafx.scene.layout.Region.USE_PREF_SIZE;

public class Sell_Pane extends Parent {

    private boolean ALLOW_SELL = true;

    private Inventory inventory;
    private Item activeItem;

    public final QueueDisplay queueDisplay;

    private final Pane main, itemInner;
    private final ScrollPane itemDisplay;
    private final Button    sell,
                    reloadInventory,
                    stop,
                    all,
                    add,
                    clear;
    private final TextArea  price,
                    amount;
    private final Label nameLabel,
                priceLabel,
                amountLabel;
    private final Rectangle square;
    private final Activity_Log activityLog;


    public Sell_Pane(){

        main = new Pane();
        main.setLayoutX(15);
        main.setLayoutY(35);
        main.setPrefSize(890, 650);
        main.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        main.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        itemInner = new Pane();
        itemInner.setPrefWidth(300);
        itemInner.setMinWidth(USE_PREF_SIZE);
        itemInner.setMaxWidth(USE_PREF_SIZE);

        itemDisplay = new ScrollPane();
        itemDisplay.setPrefSize(300, 445);
        itemDisplay.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        itemDisplay.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        itemDisplay.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        queueDisplay = new QueueDisplay();
        queueDisplay.setLayoutX(315);
        queueDisplay.setLayoutY(185);
        queueDisplay.setPrefSize(575, 260);
        queueDisplay.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        queueDisplay.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        queueDisplay.setPlaceholder(new Label("You have no items in queue."));

        activityLog = new Activity_Log();
        activityLog.setLayoutY(515);
        activityLog.setPrefSize(890, 150);
        activityLog.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        activityLog.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        sell = new Button("Sell");
        sell.setLayoutX(315);
        sell.setLayoutY(460);
        sell.setPrefSize(510, 40);
        sell.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        sell.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        sell.setOnMouseClicked(e -> {
            ALLOW_SELL = true;
            sellQueue();
        });

        reloadInventory = new Button("Load");
        reloadInventory.setLayoutX(0);
        reloadInventory.setLayoutY(460);
        reloadInventory.setPrefSize(300, 40);
        reloadInventory.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        reloadInventory.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        final boolean[] clickedOnce = {false};
        reloadInventory.setOnMouseClicked(e -> {
            try {
                if (!clickedOnce[0]){
                    reloadInventory.setText("Reload");
                    clickedOnce[0] = true;
                }
                reloadInv();
            } catch (IOException | JSONException e1) {
                e1.printStackTrace();
            }
        });

        stop = new Button("Stop");
        stop.setLayoutX(840);
        stop.setLayoutY(460);
        stop.setPrefSize(50, 40);
        stop.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        stop.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        stop.setOnMouseClicked(e -> ALLOW_SELL = false);

        all = new Button("All");
        all.setLayoutX(850);
        all.setLayoutY(80);
        all.setPrefWidth(40);
        all.setMinWidth(USE_PREF_SIZE);
        all.setMaxWidth(USE_PREF_SIZE);
        all.setOnMouseClicked(e -> {
            getItemCount();
        });

        add = new Button("Add to queue");
        add.setLayoutX(315);
        add.setLayoutY(145);
        add.setPrefWidth(510);
        add.setMinWidth(USE_PREF_SIZE);
        add.setMaxWidth(USE_PREF_SIZE);
        add.setOnMouseClicked(e -> addToQueue());

        clear = new Button("Clear");
        clear.setLayoutX(840);
        clear.setLayoutY(145);
        clear.setPrefWidth(50);
        clear.setMinWidth(USE_PREF_SIZE);
        clear.setMaxWidth(USE_PREF_SIZE);
        clear.setOnMouseClicked(e -> queueDisplay.clear());

        price = new TextArea();
        price.setLayoutX(735);
        price.setLayoutY(40);
        price.setPrefSize(100, 25);
        price.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        price.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        amount = new TextArea();
        amount.setLayoutX(735);
        amount.setLayoutY(80);
        amount.setPrefSize(100, 25);
        amount.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
        amount.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

        nameLabel = new Label("");
        nameLabel.relocate(550 , 15);

        amountLabel = new Label("Amount: ");
        amountLabel.relocate(680, 85);

        priceLabel = new Label(" Price: ");
        priceLabel.relocate(695, 45);

        square = new Rectangle(100, 100);
        square.relocate(315 ,15);

        main.getChildren().addAll(  itemDisplay,
                                    sell,
                                    reloadInventory,
                                    activityLog,
                                    stop,
                                    all,
                                    add,
                                    clear,
                                    price,
                                    amount,
                                    queueDisplay,
                                    nameLabel,
                                    amountLabel,
                                    priceLabel,
                                    square);

        this.getChildren().add(main);

    }

    public void reloadInv() throws IOException, JSONException {
        itemInner.getChildren().clear();
        inventory = new Inventory(Web_Connections.getInventory(Settings.STEAM_API_KEY, Settings.STEAM64_ID));
        if (inventory.size() != 0){
            int counter = 0;
            for (Item i : inventory.ITEMS.values()){
                ItemFragment f = new ItemFragment(i);
                f.setLayoutY(40*counter);
                itemInner.getChildren().add(f);
                counter++;
            }
            itemDisplay.setContent(itemInner);
        }
    }

    public void displayItem(Item item){
        //TODO: Make it so the picture is loaded too (Eventually)
        activeItem = item;
        nameLabel.setText(item.getName());

    }

    public void addToQueue(){
        if (amount.getText().isEmpty() || price.getText().isEmpty()){
            /*
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error");
            alert.setHeaderText("Invalid sale parameters");
            alert.setContentText("You must enter a price and amount before adding an item to the queue!");
            alert.showAndWait();
            */
        } else {
            int amt = Integer.parseInt(amount.getText());
            int amountInInv = inventory.COUNT.get(activeItem.getName());
            int amountInQueue = queueDisplay.getCount(activeItem.getName());

            // Verify that there are some left that you can add to queue
            if ((amt + amountInQueue) > amountInInv){
                /*
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("");
                alert.setHeaderText(null);
                alert.setContentText("The amount you are trying to add exceeds the amount of items you own. Ill figure out how to word the rest of this later. Basically the amount in queue is the amount you own.");
                alert.showAndWait();
                */
                amt = amountInInv-amountInQueue;

            }

            System.out.println(amt);
            if (amt <= amountInInv){

                activeItem.setPrice(Integer.parseInt(price.getText()));
                if (amt == 1) queueDisplay.addItem(activeItem);
                else if (amt > 1)queueDisplay.addAllItems(activeItem, amt);

            } else {
                /*
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Error");
                alert.setHeaderText("Invalid sale parameters");
                alert.setContentText("The amount specified exceeds the the amount of items you own!");
                alert.showAndWait();
                */
            }


        }

    }

    public void getItemCount(){
        if (nameLabel.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error");
            alert.setHeaderText("No item selected");
            alert.setContentText("You must select an item before pressing this button!");
            alert.showAndWait();
        } else {
            int i = inventory.COUNT.get(nameLabel.getText());
            amount.setText(Integer.toString(i));
        }
    }

    public void sellQueue(){

        ObservableList<Item> queue = queueDisplay.getQueue();

        while (queue.size() > 20 && ALLOW_SELL) { // Sell everything you ca in 20 item groups

            sellXItems(queue, 20);
            queue.remove(0,20);

        }

        if (ALLOW_SELL) sellXItems(queue, queue.size()); // Sell overflow

    }

    public void sellXItems(ObservableList<Item> q, int x){

        JSONArray items = new JSONArray();
        int arrayCount = 0;

        // Getting 20 items at a time, adding to json array
        for (int j = 0; j < x; j++) {

            Item i = q.get(j);
            activityLog.addUpdate(new Update(new Date(), i, Update_Type.OFFER, i.getPrice()));

            try {

                JSONObject object = new JSONObject();
                object.put("appid", "730");
                object.put("contextid", "2");
                object.put("assetid", i.getId());
                object.put("price", i.getPrice());

                items.put(arrayCount);
                arrayCount++;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        // Sell the array you just made
        JSONObject response = Web_Connections.sellItemsOPSkins(items);

        try {

            assert response != null;
            if (response.getBoolean("success")){

                String  botID = response.getString("bot_id64"),
                        tradeofferID = response.getString("tradeoffer_id");

                boolean success = Web_Connections.acceptTrade(tradeofferID, botID);

                activityLog.addUpdate(new Update(new Date(),Update_Type.TRADE,botID,Boolean.toString(success)));

            } else {
                System.out.println(response.getString("error"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void cleanActivityLog(){
        //TODO
    }

    class ItemFragment extends Parent {

        private final Item item;
        private final Rectangle container;
        private final Label amount,
                            name;

        public ItemFragment(Item item){
            this.item = item;

            container = new Rectangle(300,40);
            container.setFill(Color.LIGHTSLATEGRAY);
            container.setStroke(Color.BLACK);
            container.setOnMouseEntered(e -> {
                container.setFill(Color.LIGHTSLATEGRAY.brighter());
            });
            container.setOnMouseExited(e -> {
                container.setFill(Color.LIGHTSLATEGRAY);
            });
            container.setOnMouseClicked(e -> displayItem(this.item));

            name = new Label(item.getName());
            amount = new Label();
            this.getChildren().addAll(container, name);
        }

    }

    class QueueDisplay extends TableView<Item> {

        private final ObservableList<Item> queue = FXCollections.observableArrayList();

        private final TableColumn delete = new TableColumn("");
        private final TableColumn name = new TableColumn("Name");
        private final TableColumn price = new TableColumn("Price");

        public QueueDisplay (){
            super();

            delete.setMinWidth(30);
            delete.setMaxWidth(30);
            delete.setCellFactory(col -> {
                Button editButton = new Button("x");
                editButton.setPrefSize(20, 20);
                TableCell<Item, Item> cell = new TableCell<Item, Item>() {
                    @Override
                    public void updateItem(Item item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(editButton);
                        }
                    }
                };

                editButton.setOnMouseClicked(e -> {
                    removeItem(cell.getTableRow().getIndex());
                });

                return cell ;
            });

            name.setMinWidth(470);
            name.setMaxWidth(470);
            name.setCellValueFactory(new PropertyValueFactory<>("name"));

            price.setMinWidth(75);
            price.setMaxWidth(75);
            price.setCellValueFactory(new PropertyValueFactory<>("price"));

            this.setItems(queue);
            this.getColumns().addAll(delete, name, price);
            this.setEditable(false);
        }

        public void addItem(Item item){
            queue.add(item);
        }

        public void addAllItems(Item item, int amt){
            for (int i = 0; i < amt; i++) {
                queue.add(item);
            }

        }

        public void removeItem(int index) {
            queue.remove(index);
        }

        public void clear(){
            queue.clear();
        }

        public int getCount(String name){
            int count = 0;
            for (Item i : queue){
                if (i.getName().equals(name)) count++;
            }
            return count;
        }

        public ObservableList<Item> getQueue(){
            return queue;
        }

    }

}

