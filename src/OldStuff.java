
import Utility.Settings;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by Lachlan Stevens on 30/04/2016 at 9:56 PM.
 */
public class OldStuff {
    /*

    public static JSONObject steamLogin(String  myLoginKey) throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        // https://github.com/SteamDatabase/SteamTracking/blob/master/API/ISteamUserAuth.json

        SecureRandom rng = new SecureRandom();
        byte[] sessionID = rng.generateSeed(12);

        // Create 32 bytes of cryptographically sound random data

        byte[] aesSessionKey  = rng.generateSeed(32);

        // Asymmetrically encrypt the new session key with the public key

        byte[] cryptedSessionKey = Crypto.AESEncrypt(aesSessionKey, Crypto.publicKey);

        // Convert login key to byte array

        byte[] loginKey = myLoginKey.getBytes();

        // Symmetrically encrypt the login key with the session key

        byte[] cryptedLoginKey = Crypto.Symmetric_Encrypt(loginKey, aesSessionKey);

        // Attempt to authenticate and log in

        String query =  "steamid=" + Settings.STEAM64_ID +
                "&sessionkey=" + URLEncoder.encode(new String(cryptedSessionKey, "UTF-8"), "UTF-8") +
                "&encrypted_loginkey" + URLEncoder.encode(new String(cryptedLoginKey, "UTF-8"), "UTF-8");

        // Test output ===
        System.out.println(query);
        // ===

        JSONObject response = Web_Connections.fetch(new URL("http://api.steampowered.com/ISteamUserAuth/AuthenticateUser/v0001/"), POST, query);

        // Test output ===
        System.out.println("Breakpoint 1: Authentication response\n" + response.toString());
        // ===

        String  token = response.getString("token"),
                tokenSecure = response.getString("tokenSecure");

        // Submit cookies after logon

        String cookies =    "sessionid=" + new String(sessionID, "UTF-8") + "; " +
                "steamLogin=" + token + "; " +
                "steamLoginSecure=" + tokenSecure;

        // Test output ===
        System.out.println(cookies);
        // ===

        JSONObject cookieResponse = postWithCookies(new URL("https://steamcommunity.com/"), cookies);

        // Test output ===
        System.out.println("Breakpoint 2: Response from cookie submission\n" + response.toString());
        // ===

        return cookieResponse;
    }

    */

}
