package Utility;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.attribute.*;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Image_Reader {

    public static Map<String, Image> ALL_IMAGES;

    static {
        if(ALL_IMAGES == null)
            ALL_IMAGES = Image_Reader.loadImages();
    }

    public static Map<String, Image> loadImages() {
        Map<String, Image> images = new HashMap<>();

        Path p = FileSystems.getDefault().getPath("src/res/");
        try {
            Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path path,
                                                 BasicFileAttributes attrs) throws IOException {
                    Image image = new Image(new FileInputStream(path.toFile()));
                    String name = path.toFile().getName();
                    images.put(name.substring(0, name.length() - 4),
                            image);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.unmodifiableMap(images);
    }

    public static Image get(String imageName){
        return ALL_IMAGES.containsKey(imageName) ? ALL_IMAGES.get(imageName) : null;
    }

}
