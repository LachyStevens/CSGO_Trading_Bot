package Utility;


import java.io.*;

import java.util.HashMap;

public class File_IO {

    private final static File DEFAULT_PATH = new File(System.getProperty("user.home")+"/.tradelogs");
    public static HashMap<String, File> FILES = new HashMap<>();

    /**
     * Maps every file in the default directory to a string (its filename) and adds it to a Hashmap
     */
    public static void loadFiles(){
        // Try to make the directory if it hasn't been made
        DEFAULT_PATH.mkdir();

        FILES.clear();

        try {
            for (File file : DEFAULT_PATH.listFiles()) FILES.put(file.getName(), file);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

    /**
     * Attempts to save a file in the default directory. If the file does not exist, then that file will be created.
     * @param fileName Name of the file to save
     * @param content The content of the file in text format.
     */
    public static void saveFile(String fileName, String content){
        File file = new File(DEFAULT_PATH+File.separator+fileName);

        try {
            if (!file.exists()) file.createNewFile();

            PrintWriter writer =  new PrintWriter(file, "UTF-8");
            writer.print(content);
            writer.close();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            loadFiles(); // After updating the file reload all files
        }

    }

    /**
     * Attempts to open a file from the default directory. If the file does not exist, then an error message will be shown.
     * @param fileName Name of the file to open
     * @return A text file, or null if the file could not be found.
     */
    public static File openFile(String fileName){
        return FILES.containsKey(fileName) ? FILES.get(fileName) : null;
    }

    /**
     * Deletes a file from the default path
     * @param fileName Name of the file to delete
     * @return True if the file could be found and successfully deleted, False otherwise
     */
    public static boolean deleteFile(String fileName){
        File file = new File(DEFAULT_PATH+File.separator+fileName);
        if (file.exists()){
            file.delete();
            loadFiles();
            return true;
        }
        else return false;
    }

    /**
     * Appends a file
     * @param fileName The name of the file to be appended
     * @param newText The text to append to the file
     */
    public static void appendFile(String fileName, String newText){
        File file = openFile(fileName);
        String content = readFile(file)+newText;
        saveFile(fileName,content);
    }

    /**
     * Reads all the contents of a txt file into a string
     * @param file The file to read from
     * @return A string containing the full contents of the file
     *
     * TODO: Please dude god damn replace this with java.nio stuff
     *
     */
    public static String readFile(File file){
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(FILES.get(file.getName())));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            br.close();
            return sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Deletes any line from a file with certain contents
     * @param fileName Name of the file to check
     * @param identifier The string to look for when deciding what to delete
     */
    public static void deleteLinesWith(String fileName, String identifier){
        File file = openFile(fileName);

        BufferedReader br;
        StringBuilder sb = null;
        try {
            br = new BufferedReader(new FileReader(file));
            sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {

                if (!line.contains(identifier)){ // If that line doesn't start with or contain the identifier, write it to the string
                    sb.append(line);
                    sb.append("\n");
                }

                line = br.readLine();
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        saveFile(fileName,sb.toString());
    }

}