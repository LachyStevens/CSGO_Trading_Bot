package Utility;

import Types.Item;
import Types.Update_Type;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lachlan Stevens on 31/03/2016 at 11:00 PM.
 *
 * An object called update, which groups information on the date and time of update, type of update, the item and the price.
 */

public class Update {

    public Date date;
    public Item item;
    public Update_Type updateType;
    public Integer price;
    public String botID;
    public String message;

    // Constructor for
    public Update(Date date, Item item, Update_Type updateType, Integer price){
        this.date = date;
        this.item = item;
        this.updateType = updateType;
        this.price = price;
        this.botID = "";
    }

    public Update(Date date, Update_Type updateType, String botId, String message){
        this.date = date;
        this.updateType = updateType;
        this.botID = botId;
        this.message = message;
    }

    @Override
    public String toString(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("[dd/MM/yyyy hh.mm.ss a]");

        switch (updateType){
            case TRADE: return dateFormat.format(date) + " " + updateType.action + "with bot id " + botID + "\nThe trade was a " + message;
            default: return dateFormat.format(date) + " " + updateType.action + " \"" + item.getName() + "\" for " + price + "cents";
        }

    }

}
