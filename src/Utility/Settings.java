package Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Lachlan Stevens on 1/04/2016 at 8:53 PM.
 *
 * A place to define and store important variables
 */
public class Settings {

    public static String VERSION = "v. Alpha 0.0";

    public static String STEAM_API_KEY = "";
    public static String OPSKINS_API_KEY = "";
    public static String STEAM64_ID = "";

    public static String USERNAME = "";
    public static String PASSWORD = "";

    public static HashMap<String, String> schemaNameTranslation = new HashMap<>();

    //TODO: automate this process, its fine for now
    static {
        schemaNameTranslation.put("weapon_deagle","Desert Eagle");
        schemaNameTranslation.put("weapon_elite","Dual Berettas");
        schemaNameTranslation.put("weapon_fiveseven","Five-SeveN");
        schemaNameTranslation.put("weapon_glock","Glock-18");
        schemaNameTranslation.put("weapon_ak47","AK-47");
        schemaNameTranslation.put("weapon_aug","AUG");
        schemaNameTranslation.put("weapon_awp","AWP");
        schemaNameTranslation.put("weapon_famas","Famas");
        schemaNameTranslation.put("weapon_g3sg1","G3SG1");
        schemaNameTranslation.put("weapon_galilar","Galil AR");
        schemaNameTranslation.put("weapon_m249","M249");
        schemaNameTranslation.put("weapon_m4a1","M4A4");
        schemaNameTranslation.put("weapon_m4a1_silencer","M4A1-S");
        schemaNameTranslation.put("weapon_mac10","MAC-10");
        schemaNameTranslation.put("weapon_ump45","UMP-45");
        schemaNameTranslation.put("weapon_xm1014","XM1014");
        schemaNameTranslation.put("weapon_bizon","PP-Bizon");
        schemaNameTranslation.put("weapon_mag7","MAG-7");
        schemaNameTranslation.put("weapon_negev","Negev");
        schemaNameTranslation.put("weapon_sawedoff","Sawed-Off");
        schemaNameTranslation.put("weapon_tec9","Tec-9");
        schemaNameTranslation.put("weapon_taser","Zeus x27");
        schemaNameTranslation.put("weapon_hkp2000","P2000");
        schemaNameTranslation.put("weapon_mp7","MP7");
        schemaNameTranslation.put("weapon_mp9","MP9");
        schemaNameTranslation.put("weapon_nova","Nova");
        schemaNameTranslation.put("weapon_p250","P250");
        schemaNameTranslation.put("weapon_scar20","SCAR-20");
        schemaNameTranslation.put("weapon_sg556","SG 553");
        schemaNameTranslation.put("weapon_ssg08","SSG 08");
        schemaNameTranslation.put("weapon_c4","C4");
        schemaNameTranslation.put("weapon_cz75a","CZ75 Auto");
        schemaNameTranslation.put("weapon_revolver","R8 Revolver");
        schemaNameTranslation.put("weapon_bayonet","Bayonet");
        schemaNameTranslation.put("weapon_knife_flip","Flip Knife");
        schemaNameTranslation.put("weapon_knife_gut","Gut Knife");
        schemaNameTranslation.put("weapon_knife_karambit","Karambit");
        schemaNameTranslation.put("weapon_knife_m9_bayonet","M9 Bayonet");
        schemaNameTranslation.put("weapon_knife_tactical","Huntsman Knife");
        schemaNameTranslation.put("weapon_knife_falchion","Falchion Knife");
        schemaNameTranslation.put("weapon_knife_survival_bowie","Bowie Knife");
        schemaNameTranslation.put("weapon_knife_push","Shadow Daggers");
        schemaNameTranslation.put("Weapon Case Key","CSGO Case Key");
        schemaNameTranslation.put("E-Sports Weapon Case Key 1","eSports Key");
        schemaNameTranslation.put("Sticker Crate Key","CSGO Capsule Key");
        schemaNameTranslation.put("Community Case Key 1","Winter Offensive Case Key");
        schemaNameTranslation.put("Community Case Key 2","Operation Phoenix Case Key");
        schemaNameTranslation.put("Community Sticker Capsule 1 Key May 2014","Community Sticker Capsule 1 Key");
        schemaNameTranslation.put("Community Case Key 3 May 2014","Huntsman Case Key");
        schemaNameTranslation.put("Community Case Key 3 June 2014","Huntsman Case Key");
        schemaNameTranslation.put("Community Sticker Capsule 1 Key June 2014","Community Sticker Capsule 1 Key");
        schemaNameTranslation.put("Community Case Key 4 July 2014","Operation Breakout Case Key");
        schemaNameTranslation.put("Community Case Key 4 August 2014","Operation Breakout Case Key");
        schemaNameTranslation.put("Community Case Key 4 September 2014","Operation Breakout Case Key");
        schemaNameTranslation.put("Community Case Key 4","Operation Breakout Case Key");
        schemaNameTranslation.put("Community Case Key 5","Operation Vanguard Case Key");
        schemaNameTranslation.put("Community Case Key 6","Chroma Case Key");
        schemaNameTranslation.put("Community Case Key 7","Chroma 2 Case Key");
        schemaNameTranslation.put("Falchion Case Key","Falchion Case Key");
        schemaNameTranslation.put("Community Case Key 9","Shadow Case Key");
        schemaNameTranslation.put("crate_community_10 Key","Revolver Case Key");
        schemaNameTranslation.put("community_11 Key","Operation Wildfire Case Key");

    }

    public static void save(){
        JSONObject obj = new JSONObject();
        try {

            obj.put("steamapikey", STEAM_API_KEY);
            obj.put("steam64id", STEAM64_ID);
            obj.put("opskinsapikey", OPSKINS_API_KEY);
            obj.put("username", USERNAME);
            obj.put("password", PASSWORD);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        File_IO.saveFile("Settings", obj.toString());

    }

    public static void load(){
        JSONObject settings = null;

        try {

            if (File_IO.FILES.containsKey("Settings")){

                File f = File_IO.openFile("Settings");

                settings = new JSONObject(File_IO.readFile(f));

                STEAM_API_KEY = settings.getString("steamapikey");
                STEAM64_ID = settings.getString("steam64id");
                OPSKINS_API_KEY = settings.getString("opskinsapikey");
                USERNAME = settings.getString("username");
                PASSWORD = settings.getString("password");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String translateSchemaName(String schemaName) {
        if (schemaNameTranslation.containsKey(schemaName)) return schemaNameTranslation.get(schemaName);
        else return schemaName;
    }
}
