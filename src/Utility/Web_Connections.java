package Utility;

import Types.Item;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Created by Lachlan Stevens on 3/04/2016 at 2:37 PM.
 */
public class Web_Connections {

    public static final String GET = "GET";
    public static final String POST = "POST";


    /**
     *
     * @param url The endpoint url
     * @param requestMethod POST or GET
     * @param args Depending on what is needed, args will be a JSONObject.toString() or a URL Encoded string
     * @return The endpoint response in JSONObject form
     * @throws IOException
     * @throws JSONException
     */
    public static JSONObject fetch(URL url, String requestMethod, String args) throws IOException, JSONException {

        // Initialize a HTTPS Connection with the url
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        if (requestMethod.equals(GET) || requestMethod.equals(POST)) httpConnection.setRequestMethod(requestMethod);

        if (requestMethod.equals(POST)){

            httpConnection.setRequestProperty("Content-length", String.valueOf(args.length()));
            httpConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            httpConnection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");

        }

        httpConnection.setDoOutput(true);
        httpConnection.setDoInput(true);

        // If its a post write its arguments to the endpoint
        if (requestMethod.equals(POST)){
            DataOutputStream wr = new DataOutputStream(httpConnection.getOutputStream());
            wr.writeBytes(args);
            wr.flush();
            wr.close();
        }

        // Get the response from the endpoint
        BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        // Disconnect from the endpoint
        httpConnection.disconnect();

        return new JSONObject(sb.toString());

    }

    public static JSONObject postWithCookies(URL url, String formattedCookies) throws IOException, JSONException {

        // Initialize a HTTPS Connection with the url
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod(POST);
        httpConnection.setRequestProperty("Cookie","cookies");
        httpConnection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        httpConnection.setDoOutput(true);
        httpConnection.setDoInput(true);


        DataOutputStream wr = new DataOutputStream(httpConnection.getOutputStream());
        wr.writeBytes(formattedCookies);
        wr.flush();
        wr.close();


        // Get the response from the endpoint
        BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        // Disconnect from the endpoint
        httpConnection.disconnect();

        return new JSONObject(sb.toString());

    }

    public static CookieManager fetchCookies(URL url){


        final String COOKIES_HEADER = "Set-Cookie";
        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CookieManager msCookieManager = new java.net.CookieManager();

        Map<String, List<String>> headerFields = connection.getHeaderFields();
        List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

        // Debug code
        for (String s : cookiesHeader){
            System.out.println(s);
        }

        if(cookiesHeader != null)
        {
            for (String cookie : cookiesHeader)
            {
                msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
            }
        }

        return msCookieManager;

    }

    public static Image fetchImage(String url) throws IOException {
        return new Image(url);
    }

    public static String verifyCaptcha(Image img){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Captcha Verification");
        dialog.setHeaderText("");
        dialog.setContentText("Please enter the Captcha:");


        ImageView view = new ImageView(img);
        view.setFitWidth(img.getWidth());
        view.setFitHeight(img.getHeight());
        view.setX(20);
        view.setY(20);

        dialog.setGraphic(view);

        Optional<String> result = dialog.showAndWait();
        final String[] c = {""};
        result.ifPresent(captcha -> c[0] = captcha);

        return c[0];
    }

    // Steam API functions

    // Because apparently the steam api is a piece of shit, I have to continually ping the endpoint for a response until I get one.
    // Fucking Gaben.
    public static JSONObject getInventory(String apiKey, String steam64ID) throws IOException, JSONException {
        JSONObject jsonObjectResults = null;
        int counter = 0;

        while (counter < 100) {
            try {
                jsonObjectResults = fetch(new URL("http://api.steampowered.com/IEconItems_730/GetPlayerItems/v0001/?key=" + apiKey + "&SteamID=" + steam64ID), GET, null);
            } catch (IOException e) {
                counter++;
            }

            if (jsonObjectResults != null) break;
        }


        return (counter == 100) ? null : jsonObjectResults;
    }

    // OPSkins API functions

    public static JSONObject sellItemsOPSkins(JSONArray items){
        String query = null;
        URL url = null;

        try {

            query = "request=SellItems&key=" + Settings.OPSKINS_API_KEY + "&items=" + items.toString();
            url = new URL("https://opskins.com/api/user_api.php");

            return fetch(url, POST, query);

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    public static JSONArray listToJSONArray(ArrayList<Item> items) throws JSONException{
        JSONArray array = new JSONArray();
        int counter = 0;

        for (Item i : items){
            JSONObject object = new JSONObject();
            object.put("appid", "730");
            object.put("contextid", "2");
            object.put("assetid", i.getId());
            object.put("price", i.getPrice());

            array.put(counter, object);
            counter++;
        }

        return array;
    }

    // Other

    public static boolean acceptTrade(String tradeID, String sessionID){

        String query =  "sessionid=" + sessionID +
                            "&tradeofferid=" + tradeID;

        JSONObject response = null;

        try {
            response = fetch(new URL("http://steamcommunity.com/tradeoffer/" + tradeID + "/accept"), POST, query);
        } catch (IOException e) {
            e.printStackTrace();
        }



        return false;
    }


}
